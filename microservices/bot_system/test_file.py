from PIL import ImageFont

def get_total_area(width, height):
    return width * height

def get_font(name, size):
    return ImageFont.truetype(name, size)

def fit(width, height, font, size, text):
    font = get_font(font, size)
    text_w, text_h = font.getsize(text)
    text_area = get_total_area(text_w, text_h)
    total_area = get_total_area(width, height)
    if total_area>text_area:
        return True
    else:
        scale = total_area/text_area
        size = int(round(size*scale))
        return size

def calculate(x, y, text):
    size = 100
    font = 'pirates.otf'
    font_init = get_font(font, size)
    fw, fh = font_init.getsize(text)
    x = x * 0.9
    y = y * 0.9
    ratio = (fw*fh)/(x*y)
    if ratio > 1:
       est_font_size = size // (ratio+ratio*0.1)
    else:
        ratio = (x*y)/(fw*fh)
        est_font_size = size * (ratio - ratio*0.1)
    print(fw*fh)
    print(x*y)
    print(ratio)
    print(est_font_size)
    return  int(est_font_size)