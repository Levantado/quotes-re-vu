from os import environ
import aiopg
import asyncio
import sys
import random
from PIL import Image, ImageDraw, ImageFont, ImageFilter
from io import BytesIO
import pprint
# from image_taker import image_taker_method
import textwrap
from test_file import calculate

if sys.platform == "win32":
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
host = environ.get("DB_HOST", "localhost")
dsn = f"dbname=postgres user=postgres password=postgres host={host}"


async def get_data():
    async with aiopg.create_pool(dsn) as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                try:
                    await cur.execute("SELECT count(*) from quotes")
                    async for row in cur:
                        lenght = row[0]
                    await cur.execute(
                        f"SELECT author_name, quote_text from quotes join authors a on a.author_id = quotes.quote_auth_id where quote_id={random.randrange(lenght)}"
                    )
                    async for row in cur:
                        data = {"author": row[0], "text": row[1]}
                        return data
                except Exception as e:
                    print(e)

def get_dominant_color(pil_img):
    img = pil_img.copy()
    img.convert("RGB")
    img.resize((1, 1), resample=0)
    dominant_color = img.getpixel((0, 0))
    return dominant_color

# async def drawing(quote, file):
async def drawing(quote):
    r,g,b = 255,255,255
    x,y = random.randint(400,1200), random.randint(200, 800)
    rr,gr,br = random.randint(0,255), random.randint(0,255) ,random.randint(0,255)
    img= Image.new('RGB', (x,y), color=(rr,gr,br))
    img.write_text_box()
    # img = Image.open(file)
    ccc = get_dominant_color(img)
    l = (0.299*ccc[0] + 0.587*ccc[1]+0.114*ccc[2] )/255
    if l > 0.5:
        r,g,b = 0, 0 ,0

    if r ==0:
       t = 255
    else:
        t = 0

    img = img.filter(ImageFilter.GaussianBlur(8))

    width, height = img.size
    size = calculate(width,height, quote['text'])

    fnt = ImageFont.truetype('pirates.otf',size)
    d = ImageDraw.Draw(img)
    a = width//size
    letter = fnt.getsize('i')
    text_a= fnt.getsize(quote['text'])
    wsize = int(width)//letter[0]
    stats = {'luminati': l,
            'wsize': wsize,
             'width/size': a,
             'letter_size': letter,
             'font_size': size,
             'text_length': len(quote['text']),
             'text_demention': text_a,
             'text_area': text_a[0]*text_a[1],
             'image_area': width*height,
             'image_width':width,
             'image_height': height}

    pprint.pprint(stats)
    margin = width * 0.1
    offset = height * 0.2
    #
    # for line in textwrap.wrap(quote['text'], width=wsize):
    #     d.text((margin+1,offset), line, font=fnt, fill=(t, t, t))
    #     d.text((margin,offset+1), line, font=fnt, fill=(t, t, t))
    #     d.text((margin-1,offset), line, font=fnt, fill=(t, t, t))
    #     d.text((margin,offset-1), line, font=fnt, fill=(t, t, t))
    #     d.text((margin,offset), line, font=fnt, fill=(r, g, b))
    #     offset += fnt.getsize(line)[1]
    # fnt = ImageFont.truetype('pirates.otf',size*80//100)
    # print(quote['author'], len(quote['author']))
    # whitespace = width - fnt.getsize(quote['author'])[0]-50
    # d.text((whitespace+1,offset + (height- offset)/2), quote['author'], font=fnt, fill=(t,t, t))
    # d.text((whitespace-1,offset + (height- offset)/2), quote['author'], font=fnt, fill=(t,t, t))
    # d.text((whitespace,1+offset + (height- offset)/2), quote['author'], font=fnt, fill=(t,t, t))
    # d.text((whitespace,-1+offset + (height- offset)/2), quote['author'], font=fnt, fill=(t,t, t))
    # d.text((whitespace,offset + (height- offset)/2), quote['author'], font=fnt, fill=(r,g, b))
    # temp = BytesIO()
    # img.save(temp, format='png')
    img.save('temp.png', format='png')
    temp = 0
    return temp


async def generate_image(loop):
    quote = None
    while quote is None:
        quote = await get_data()

    pprint.pprint(quote)
    # file = await image_taker_method(loop)
    # return await drawing(quote, file), quote
    return await drawing(quote), quote


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(generate_image(loop))
