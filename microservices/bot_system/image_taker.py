import configparser
import random

from telethon import TelegramClient
from io import BytesIO
from telethon.tl.functions.messages import (GetHistoryRequest)


config = configparser.ConfigParser()
config.read("config.ini")
api_id = config['Telegram']['api_id']
api_hash = config['Telegram']['api_hash']

api_hash = str(api_hash)
api_id = int(api_id)

phone = config['Telegram']['phone']
username = config['Telegram']['username']

channel_name = 'https://t.me/avochki'

async def image_taker_method(loop):
    client = TelegramClient(username, api_id, api_hash, loop=loop)
    await client.start()
    channel_entity = await client.get_entity(channel_name)
    id = random.randrange(1000,26000)
    posts = await client(GetHistoryRequest(
        peer=channel_entity,
        limit=1,
        offset_date=None,
        offset_id=id,
        max_id=0,
        min_id=0,
        add_offset=0,
        hash=0))
    print('Get data from channel')
    file = BytesIO()
    for x in posts.messages:
        await client.download_media(x.media, file)
    return file

