import asyncio
import logging
from os import environ
from image_generator import generate_image

from aiogram import Bot

API_TOKEN = environ.get('API_TOKEN')
CHANNEL_ID = int(environ.get('CHANNEL_ID'))
logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)


async def send_message(channel_id: int, image, quote):
        await bot.send_photo(channel_id, image, caption=f"`{quote['text']}` - *Author*: _{quote['author']}_", parse_mode='markdown')


async def main(loop):
    while True:
        # await asyncio.sleep(5)
        file, quote = await generate_image(loop)
        await send_message(CHANNEL_ID, file.getvalue(), quote)
        break


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
