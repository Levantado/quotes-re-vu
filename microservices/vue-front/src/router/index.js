import Vue from "vue";
import VueRouter from "vue-router";
import QuoteList from "../components/QuoteList";
import Statistics from "../components/Statistics";
import Scrapers from "../components/Scrapers";

Vue.use(VueRouter);

const routes = [
  { component: QuoteList, path: "/" },
  { component: Statistics, path: "/statistics" },
  { component: Scrapers, path: "/scrapers" },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
