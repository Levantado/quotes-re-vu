import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";

let url = process.env.VUE_APP_URL_BACKEND;
console.log(url);
axios.defaults.baseURL = url;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
