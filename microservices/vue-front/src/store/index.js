import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

let BACKEND_URI = process.env.VUE_APP_URL_BACKEND;
console.log(BACKEND_URI);
export default new Vuex.Store({
  state: {
    quotes: [],
    tag: "love",
    quotesSize: 5,
    tagRating: [],
    authorRating: [],
    scrapers: {},
  },
  mutations: {
    storeQuotes(state, quotesData) {
      state.quotes = quotesData;
    },
    storeTag(state, tagData) {
      state.tag = tagData;
    },
    storeQuoteSize(state, quoteSize) {
      state.quotesSize = quoteSize;
    },
    storeTagRating(state, tagRating) {
      state.tagRating = tagRating;
    },
    storeAuthorRating(state, authorRating) {
      state.authorRating = authorRating;
    },
    storeScraperStatus(state, scraperStatusInfo) {
      Vue.set(
        state.scrapers,
        scraperStatusInfo["id"],
        scraperStatusInfo["status"]
      );
    },
  },
  actions: {
    sseStart({ commit }) {
      let eventSource = new EventSource(BACKEND_URI + "/api/admin/stream");
      eventSource.addEventListener(
        "message",
        function (event) {
          var scraperStatus = JSON.parse(event.data);
          commit("storeScraperStatus", scraperStatus);
        },
        false
      );
      eventSource.addEventListener(
        "error",
        (event) => {
          console.log("SOME ERROR", event);
        },
        false
      );
    },

    getQuotes({ commit }, payload = "tag") {
      axios
        .get(
          `/api/quotes/${payload}/${this.state.tag}/${this.state.quotesSize}`
        )
        .then((results) => {
          commit("storeQuotes", results.data);
        });
    },
    setTag({ commit, dispatch }, payload) {
      commit("storeTag", payload);
      dispatch("getQuotes");
    },
    setQuoteSize({ commit, dispatch }, payload) {
      commit("storeQuoteSize", payload);
      dispatch("getQuotes");
    },
    getQuotesByAuthor({ commit }, payload) {
      axios
        .get(`/api/quotes/author/${payload}/${this.state.quotesSize}`)
        .then((results) => {
          commit("storeQuotes", results.data);
        });
    },
    getTagRating({ commit }) {
      axios.get("/api/quotes/statistics/tags").then((results) => {
        commit("storeTagRating", results.data);
      });
    },
    getAuthorRating({ commit }) {
      axios.get("/api/quotes/statistics/authors").then((results) => {
        commit("storeAuthorRating", results.data);
      });
    },
  },
  getters: {
    quotes(state) {
      if (state.quotes.length === 0) {
        return [
          {
            author: "John Doe",
            text: "Another empty quotes",
            picture_src:
              "https://www.pngitem.com/pimgs/m/557-5578368_empty-profile-picture-icon-hd-png-download.png",
          },
        ];
      }
      return state.quotes;
    },
    tag(state) {
      return state.tag;
    },
    quoteSize(state) {
      return state.quotesSize;
    },
    tagRating(state) {
      return state.tagRating;
    },
    authorRating(state) {
      return state.authorRating;
    },
    scrapersStatus(state) {
      return state.scrapers;
    },
  },
  modules: {},
});
