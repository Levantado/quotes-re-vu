import redis
from flask import Blueprint, Response, current_app, jsonify
from sqlalchemy import func, text

from app.utils.db import db

from .models import Authors, Quotes, Tags

api_bp = Blueprint("api_bp", __name__)


def generate_quotes(results):
    quotes_output = []
    for x in results:
        tag_list = [y.tag_name for y in x.tags_collection]
        quote = {
            "text": x.quote_text,
            "author": x.authors.author_name,
            "picture_src": x.authors.author_pic,
            "tags": tag_list,
            "likes": x.quote_likes,
        }
        quotes_output.append(quote)
    return quotes_output


@api_bp.route("/ping")
def ping():
    return jsonify({"message": "Pong"})


@api_bp.route("/quotes/tag/<tag_q>/<int:number>")
def tag(tag_q="love", number=50):
    results = (
        Quotes.query.order_by(Quotes.quote_likes.desc())
        .filter(Quotes.tags_collection.any(tag_name=tag_q))
        .slice(0, number)
        .all()
    )
    quotes = generate_quotes(results)
    return jsonify(quotes)


@api_bp.route("/quotes/author/<author_q>/<int:number>")
def author(author_q="", number=50):
    results = (
        Quotes.query.order_by(Quotes.quote_likes.desc())
        .filter(Quotes.authors.has(author_name=author_q))
        .slice(0, number)
        .all()
    )
    quotes = generate_quotes(results)
    return jsonify(quotes)


@api_bp.route("/quotes/statistics/tags")
def statistics_tag():
    results = (
        db.session.query(Tags, func.count(Quotes.quote_id).label("total"))
        .join(Tags.quotes_collection)
        .order_by(text("total DESC"))
        .group_by(Tags.tag_id)
        .slice(0, 100)
        .all()
    )
    tags = []
    for result in results:
        tag = {"tag_name": result[0].tag_name, "qoutes_count": result[1]}
        tags.append(tag)
    return jsonify(tags)


@api_bp.route("/quotes/statistics/authors")
def statistics_author():
    results = (
        db.session.query(Authors, func.count(Quotes.quote_id).label("total"))
        .join(Quotes)
        .order_by(text("total DESC"))
        .group_by(Authors.author_id)
        .slice(0, 100)
        .all()
    )
    authors = []
    for result in results:
        author = {
            "author": result[0].author_name,
            "picture_src": result[0].author_pic,
            "quotes_count": result[1],
        }
        authors.append(author)

    return jsonify(authors)


@api_bp.route("/admin/scrapers/status")
def scrapers_status():
    redis = current_app.red
    scrapers = []
    for key in redis.scan_iter("Scraper:*"):
        scraper = {"name": key, "status": redis.get(key)}
        scrapers.append(scraper)
    return jsonify(sorted(scrapers, key=lambda x: (int(x["name"].split(":")[-1]))))


def event_stream():
    red = redis.Redis()
    pubsub = red.pubsub()
    pubsub.subscribe("scraper")
    for message in pubsub.listen():
        mes = message["data"]
        if mes != 1:
            id_part, text_part = message["data"].decode("utf-8").split(":")
            data = f'data: {{"id":"{id_part}", "status":"{text_part}"}}\n\n'
            yield data


@api_bp.route("/admin/stream")
def stream():
    return Response(event_stream(), mimetype="text/event-stream")
