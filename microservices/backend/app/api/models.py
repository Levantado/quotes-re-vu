from sqlalchemy.ext.automap import automap_base

from app.utils.db import db

Base = automap_base(db.Model)
Base.prepare(db.engine, reflect=True)
Authors = Base.classes.authors
Quotes = Base.classes.quotes
Tags = Base.classes.tags

# db.Model.metadata.reflect(db.engine)

# class Authors(db.Model):
#     __tablename__ = 'authors'
#     # __table_args__ = {
#     #     'autoload': True,
#     #     'schema': 'public',
#     #     'autoload_with': db.engine
#     # }
#
#
# class Quotes(db.Model):
#     __tablename__ = 'quotes'
#     # __table_args__ = {
#     #     'autoload': True,
#     #     'schema': 'public',
#         # 'autoload_with': db.engine
#     # }
#
#
# class Tags(db.Model):
#     __tablename__ = 'tags'
#     # __table_args__ = {
#     #     'autoload': True,
#     #     'schema': 'public',
#         # 'autoload_with': db.engine
#     # }
#
