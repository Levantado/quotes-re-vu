import os

import redis
from flask import Flask
from flask_cors import CORS

from app.utils.db import db, migrate


def create_app():
    app = Flask(__name__)
    app.config.from_object(os.environ.get("APP_ENV", "app.config.Base"))
    app.red = redis.Redis(decode_responses="utf-8")
    CORS(app)
    db.init_app(app)
    with db.engine.connect() as con:
        with open("utils/create_db.sql", "r") as f:
            rs = con.execute("f")
            con.commit()
    db.reflect(app=app)
    migrate.init_app(app, db)
    with app.app_context():
        from app.api import api_bp
    app.register_blueprint(api_bp, url_prefix="/api")

    return app
