class Base(object):
    SECRET_KEY = "123"
    SQLALCHEMY_DATABASE_URI = "sqlite:///app.sqlite3"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class Development(Base):
    SECRET_KEY = "ABC"
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://postgres:postgres@localhost"
