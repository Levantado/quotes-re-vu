create TABLE authors(
    author_id serial PRIMARY KEY,
    author_name VARCHAR (200) UNIQUE  NOT NULL,
    author_url VARCHAR (200) NOT NULL,
    author_pic varchar (200),
    scraped BOOLEAN default FALSE
);

create TABLE quotes(
    quote_id serial PRIMARY KEY,
    quote_text text NOT NULL,
    quote_link VARCHAR(255) UNIQUE,
    quote_likes BIGINT,
    quote_auth_id INTEGER REFERENCES authors(author_id)
);

create TABLE tags(
    tag_id serial PRIMARY KEY,
    tag_name varchar(50) NOT NULL UNIQUE
);

create TABLE quoteTags(
    tag_id int references tags,
    quote_id int references quotes
    , PRIMARY KEY(quote_id, tag_id)
);
