from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise
from core.config import DATABASE_URL, MODELS
from api.base.views import base_router
from fastapi.middleware.cors import CORSMiddleware


def create_app():
    origins = ["http://localhost:8080", "http://127.0.0.1:8080", "http://localhost", "http://127.0.0.1", "http://51.68.188.133"]

    app = FastAPI(title="QuotesBackend", debug=True, version="0.2")
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    register_tortoise(
        app,
        db_url=DATABASE_URL,
        modules={"models": MODELS},
        add_exception_handlers=True,
    )
    app.include_router(base_router, prefix="/api")
    return app


app = create_app()
