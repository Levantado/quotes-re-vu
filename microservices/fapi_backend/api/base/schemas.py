from typing import List, Optional
from tortoise.contrib.pydantic import PydanticModel


class STag(PydanticModel):
    tag_id: int
    tag_name: str

    class Config:
        orm_mode = True


class SAuthor(PydanticModel):
    author_id: int
    author_name: str
    author_url: str
    author_pic: str
    scraped: bool

    class Config:
        orm_mode = True


class SQuote(PydanticModel):
    quote_id: int
    quote_text: str
    quote_link: str
    quote_likes: int
    quote_auth: SAuthor
    tags: List[STag]

    class Config:
        orm_mode = True


class SQuote2(PydanticModel):
    quote_id: int
    quote_text: str
    quote_link: str
    quote_likes: int

    class Config:
        orm_mode = True


class SAuthor2(PydanticModel):
    author_id: int
    author_name: str
    author_url: str
    author_pic: str
    scraped: bool
    total: int

    class Config:
        orm_mode = True


class STags(PydanticModel):
    tag_id: int
    tag_name: str
    total: int

    class Config:
        orm_mode = True
