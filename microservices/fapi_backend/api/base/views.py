from fastapi import APIRouter, requests
from api.base.models import Authors, Quotes, Tags
from typing import List
from api.base.schemas import SAuthor2, SQuote, STags
from core.config import REDIS_URL
from tortoise.functions import Count
from sse_starlette.sse import EventSourceResponse

base_router = APIRouter()
import aioredis


@base_router.on_event("startup")
async def combine():
    print(REDIS_URL)
    base_router.redis = await aioredis.create_redis_pool(REDIS_URL)


@base_router.on_event("shutdown")
async def decombine():
    await base_router.redis.close()


@base_router.get("/ping")
async def ping():
    return {"message": "pong", "status": 200}


@base_router.get("/quote", response_model=List[SQuote])
async def get_quote():
    result = await Quotes.all().prefetch_related("quote_auth", "tags").limit(100)
    return result


@base_router.get("/quotes/author/{id_auth}/{num_quotes}", response_model=List[SQuote])
async def get_authors(id_auth, num_quotes: int):
    result = (
        await Quotes.filter(quote_auth__author_name=id_auth)
        .all()
        .prefetch_related("quote_auth", "tags")
        .limit(num_quotes)
    )
    return result


@base_router.get("/quotes/tag/{id_tags}/{num_quotes}", response_model=List[SQuote])
async def get_authors(id_tags, num_quotes: int):
    result = (
        await Quotes.filter(tags__tag_name=id_tags)
        .all()
        .prefetch_related("quote_auth", "tags")
        .limit(num_quotes)
    )
    return result


@base_router.get("/quotes/statistics/authors", response_model=List[SAuthor2])
async def get_authors_statistics():
    result = (
        await Authors.all()
        .prefetch_related("quotes")
        .annotate(total=Count("quotes"))
        .order_by("-total")
        .limit(100)
    )
    return result


@base_router.get("/quotes/statistics/tags", response_model=List[STags])
async def get_tags_statistics():
    result = (
        await Tags.all()
        .prefetch_related("quotes")
        .annotate(total=Count("quotes"))
        .order_by("-total")
        .limit(100)
    )
    return result


@base_router.get("/qutes/task/{tag}")
async def set_task(tag):
    url = "https://www.goodreads.com/quotes/tag/"
    await base_router.redis.lpush("url", url + tag)
    return {"task": tag, "code": 200}


async def event_stream(request):
    red = base_router.redis
    sub = await red.subscribe("scraper")
    ch1 = sub[0]
    while await ch1.wait_message():
        msg = await ch1.get()
        id_part, text_part = msg.decode("utf-8").split(":")
        data = f'{{"id":"{id_part}", "status":"{text_part}"}}\n\n'
        yield data


@base_router.route("/admin/stream")
async def stream(request: requests.Request):
    event_generator = event_stream(request)
    response = EventSourceResponse(event_generator)
    return response
