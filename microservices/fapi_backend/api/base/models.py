from tortoise import Tortoise, fields, models


class Authors(models.Model):
    author_id = fields.IntField(pk=True)
    author_name = fields.CharField(max_length=200)
    author_url = fields.CharField(max_length=200)
    author_pic = fields.CharField(max_length=200)
    scraped = fields.BooleanField(default=False)

    class Meta:
        table = "authors"

    def __str__(self):
        return f"{self.author_id} | {self.author_name}"


class Quotes(models.Model):
    quote_id = fields.IntField(pk=True)
    quote_text = fields.TextField()
    quote_link = fields.CharField(max_length=255)
    quote_likes = fields.BigIntField()
    quote_auth = fields.ForeignKeyField(
        "models.Authors", related_name="quotes", to_field="author_id"
    )
    tags = fields.ManyToManyField(
        "models.Tags",
        through="quotetags",
        forward_key="tag_id",
        backward_key="quote_id",
        related_name="quotes",
    )

    class Meta:
        table = "quotes"


class Tags(models.Model):
    tag_id = fields.IntField(pk=True)
    tag_name = fields.CharField(max_length=50)


Tortoise.init_models(["api.base.models"], "models")
