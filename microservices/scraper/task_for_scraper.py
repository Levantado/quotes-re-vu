import redis


red = redis.Redis()
tag = ['love', 'death', 'life', 'friends', 'light', 'sun', 'dog', 'cat', 'romance', 'logic', 'wisdom', 'art', 'truth', 'zen', 'fight']
# tag = ['love']
url= 'https://www.goodreads.com/quotes/tag/'
# url = 'https://www.goodreads.com/author/quotes/3565.Oscar_Wilde'
for x in tag:
    red.lpush('url', url+x)

authors = [
    'https://www.goodreads.com/author/quotes/3565.Oscar_Wilde',
    'https://www.goodreads.com/author/quotes/5810891.Mahatma_Gandhi',
    'https://www.goodreads.com/author/quotes/7715.Robert_Frost'
           ]
# for x in authors:
#     red.lpush('url', x)
#
