import asyncio
import aiohttp
import pprint
import sys
import aioredis
import aiopg
from lxml import html
import datetime
from os import environ

if sys.platform == 'win32':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

count = 0
WORKERS = int(environ.get('WORKERS', 10))
host = environ.get('DB_HOST', 'localhost')
redis_uri = environ.get('REDIS_URI', 'redis://localhost')
base = 'https://www.goodreads.com'
dsn = f'dbname=postgres user=postgres password=postgres host={host}'


async def inserter(cur, dict_query):
    tags = '{' + ", ".join(dict_query['tags']) + '}'
    if '/quotes/tag/' in dict_query['author_url']:
        pprint.pprint(dict_query)
    raw_sql = rf'''
    WITH data(author_name, author_url, author_pic,  quote_text, quote_link, quote_likes, tags) AS (
        VALUES
               ('{dict_query['author']}', '{dict_query['author_url']}',  '{dict_query['picture']}', '{dict_query['text']}', '{dict_query['quote_link']}',{dict_query['likes']}, '{tags}')
    )
       , input_tags AS (
        SELECT DISTINCT tag_name
        FROM data,
             unnest(tags::text[]) tag_name
    )
       , insert_author AS (
        INSERT INTO authors (author_name, author_url, author_pic)
            SELECT author_name, author_url, author_pic FROM data
            ON CONFLICT (author_name) DO UPDATE SET author_name = excluded.author_name 
            RETURNING author_name, author_id AS author_id
    )
       , insert_quote AS (
        INSERT INTO quotes (quote_text, quote_link, quote_likes, quote_auth_id)
            SELECT quote_text, quote_link, quote_likes, author_id
            FROM data
                     JOIN insert_author USING (author_name)
            ON CONFLICT (quote_link) DO NOTHING
            RETURNING quote_id
    )
       , insert_tags AS (
        insert into tags (tag_name)
            TABLE input_tags
            on conflict (tag_name) DO NOTHING
            returning tag_id
    )
    INSERT
    INTO quoteTags (quote_id,tag_id)
    select insert_quote.quote_id, insert_tags.tag_id
    from insert_quote,
         (
             select tag_id
             from insert_tags
             union all
             select tag_id
             from input_tags
                      JOIN tags USING (tag_name)
         ) insert_tags;
    '''
    try:
        await cur.execute(raw_sql)
    except Exception as e:
        print(raw_sql)
        print(e)
    return


def cleaner(text):
    cleaned_text = text.replace('\n', '').lstrip().rstrip().replace('“', '').replace('”', '').replace(',', '').replace(
        ' quotes', '').replace("'", '"''')
    return cleaned_text


def parser(url, quotes):
    quotes_list = []
    for x in quotes:
        raw = x.xpath('./div[@class="quoteText"]')[0]
        text = cleaner(raw.xpath('./text()')[0])
        if 'www.goodreads.com/author/' in url:
            url_pic = x.xpath('//h1/a/img/@src')
            name = cleaner(x.xpath("//div[@class='quotes']/h2/text()")[0])
        else:
            url_pic = x.xpath('./a/img/@src')
            name = cleaner(raw.xpath('./span/text()')[0])
        if not url_pic:
            url_pic = 'https://www.pngitem.com/pimgs/m/557-5578368_empty-profile-picture-icon-hd-png-download.png'
        else:
            url_pic = url_pic[0]
        author_url = x.xpath('./a[@class="leftAlignedImage"]/@href')
        if author_url:
            if 'author/show/' in author_url[0]:
                author_url = base + '/author/quotes/' + author_url[0].split('/')[-1]
            else:
                author_url = ''
        else:
            if '/quotes/tag' in url:
                author_url = ''
            else:
                author_url = url.split('?')[0]
        tags = x.xpath('./div/div[contains(@class,"greyText")]/a/text()')
        likes = x.xpath('./div/div[contains(@class,"right")]/a[contains(@title,"View this quote")]/text()')[0].replace(
            ' likes', '')
        quote_link = base + x.xpath('./div/div[contains(@class,"right")]/a[contains(@title,"View this quote")]/@href')[
            0]
        quote = {'author': name,
                 'text': text,
                 'tags': tags,
                 'picture': url_pic,
                 'author_url': author_url,
                 'quote_link': quote_link,
                 'likes': int(likes)}
        quotes_list.append(quote)
    return quotes_list


async def fetch(session, url, flag, name):
    async with session.get(url) as response:
        soup = html.fromstring(await response.text())
        if not flag:
            quotes_raw = soup.xpath('//div[contains(@class,"quoteDetails")]')
            to_save = parser(url, quotes_raw)
            if to_save:
                async with aiopg.create_pool(dsn) as pool:
                    async with pool.acquire() as conn:
                        async with conn.cursor() as cur:
                            for x in to_save:
                                await inserter(cur, x)
            print(f'Time:{datetime.datetime.utcnow()}, Worker:{name}, Insert: {len(to_save)} records,  From: {url}')
        else:
            print(f'Time:{datetime.datetime.utcnow()}, Worker:{name} START getting pages links')
            a = soup.xpath('//a[@class="next_page"]/preceding::a[1]/@href')[0]
            suffix, numbers = a.split('=')
            for page_link_number in range(1, int(numbers) + 1):
                await flag.sadd('url_task', base + suffix + '=' + str(page_link_number))

            print(f'Time:{datetime.datetime.utcnow()}, Worker:{name}, END getting page links')


async def worker(redis, flag=None, name='999'):
    async with aiohttp.ClientSession() as session:
        await redis.publish('scraper', 'Scraper' + name + ':idle')
        status = 'idle'
        while True:
            if status != 'idle':
                await redis.publish('scraper', 'Scraper' + name + ':idle')
                status = 'idle'
            if flag:
                url = await redis.lpop('url', encoding='utf-8')
            else:
                url = await redis.spop('url_task', encoding='utf-8')
            if url:
                status = 'work'
                await redis.publish('scraper', 'Scraper' + name+':work')
                await fetch(session, url, flag, name)
            else:
                await asyncio.sleep(2)


async def main():
    reds = await aioredis.create_redis_pool(redis_uri)
    tasks_regular = [asyncio.create_task(worker(reds, False, str(x))) for x in range(WORKERS)]
    task_getter_page = [asyncio.create_task(worker(reds, reds)) for _ in range(2)]
    tasks_regular.extend(task_getter_page)
    await asyncio.gather(*tasks_regular)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
